#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#define SRV_NUM 96
#define BUF_LEN 8000
#define PORT 5000
#define RCVBUF 16777216
#define SNDBUF 16777216

void oops(char *s)
{
    perror(s);
    exit(1);
}

void *srv()
{
    struct sockaddr_in srv_addr, cli_addr;
    int s, i, cli_addr_len = sizeof(cli_addr);
    char buf[BUF_LEN];

    if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == -1)
        oops("socket()");

    int reuse_port = 1;
    if (setsockopt(s, SOL_SOCKET, SO_REUSEPORT, &reuse_port, sizeof(reuse_port)) == -1)
        oops("setsockopt(...SO_REUSEPORT)");

    int rcvbuf = RCVBUF;
    if (setsockopt(s, SOL_SOCKET, SO_RCVBUF, &rcvbuf, sizeof(rcvbuf)) == -1)
        oops("setsockopt(...SO_RCVBUF)");

    int sndbuf = SNDBUF;
    if (setsockopt(s, SOL_SOCKET, SO_SNDBUF, &sndbuf, sizeof(sndbuf)) == -1)
        oops("setsockopt(...SO_SNDBUF)");

    memset((char *)&srv_addr, 0, sizeof(srv_addr));
    srv_addr.sin_family = AF_INET;
    srv_addr.sin_port = htons(PORT);
    srv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    if (bind(s, (struct sockaddr *)&srv_addr, sizeof(srv_addr)) == -1)
        oops("bind()");

    while (1)
    {
        int len = recvfrom(s, buf, BUF_LEN, 0, (struct sockaddr *)&cli_addr, &cli_addr_len);
        if (len == -1)
            oops("recvfrom()");

        sendto(s, buf, len, MSG_DONTWAIT, (struct sockaddr *)&cli_addr, cli_addr_len);
    }
    close(s);
    return NULL;
}

int main(void)
{
    pthread_t srv_thread[SRV_NUM];
    int i;

    for (i = 0; i < SRV_NUM; ++i)
        if (pthread_create(&srv_thread[i], NULL, srv, NULL))
            oops("pthread_create()");

    for (i = 0; i < SRV_NUM; ++i)
        if (pthread_join(srv_thread[i], NULL))
            oops("pthread_join()");

    return 0;
}
